# Bint

## Overview

*Bint* is a Rust library for handling arbitrary precision integers and rational numbers. This library provides two data types `Bint` and `Bratio` as well as a set of operations to perform arithmetic. `Bint` represents big integers, while `Bratio` represents rational numbers (fractions). This library is designed to be easy to use and efficient in terms of performance.

## Usage Examples

### Big Integer

```rust
use bint_and_bratio::Bint;

let a = Bint::from(1234567890);
let b = Bint::try_from("98765432109876543210").unwrap();

let sum = a + b;
let product = a * b;
let gcd = a.gcd(b);

println!("Sum: {sum}");
println!("Product: {product}");
println!("GCD: {gcd}");
```

```rust
let a = Bint::from(4756890213);
let b = Bint::from(438290);

let c = a.pow(b);
let d = a % b;
let e = a & b;

println!("Power: {c}");
println!("Remainder: {d}");
println!("Bit and: {e:b}");
println!("Binary a: {a:b}");
println!("Binary b: {b:b}");
```

### Rational Numbers

```rust
use bint_and_bratio::Bratio;

let a = Bratio::new(1, 3);
let b = Bratio::from(0.5);
let c = Bratio::new(20, 4);
let d = Bratio::new(355, 113);

let sum = a + b;
let product = a * b;
c.reduce();
let float: f64 = d.try_into().unwrap();

println!("Sum: {sum}");
println!("Product: {product}");
println!("Reduced fraction: {c}");
println!("Decimal: {float}");
```