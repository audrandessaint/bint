import argparse
import os
import operator 
from random import randrange
from enum import Enum

class Operations(Enum):
    ADDITION = "add"
    SUBTRACTION = "sub"
    MULTIPLICATION = "mul"
    LEFT_SHIFT = "lshift"
    RIGHT_SHIFT = "rshift"
    BIT_AND = "and"
    BIT_OR = "or"
    BIT_XOR = "xor"
    BIT_NOT = "not"
 
parser = argparse.ArgumentParser(description="Generates a dataset for benchmarking the Bint library")

parser.add_argument("-o", "--out", default="./data-dev", help="output directory path")
parser.add_argument("--name", help="output file name")

parser.add_argument("count", type=int, help="number of number pairs to generate")
parser.add_argument("--left", nargs=2, type=int, metavar="<number>", help="", default=[30, 50])
parser.add_argument("--right", nargs=2, type=int, metavar="<number>", help="", default=[30, 50])
parser.add_argument("--op", nargs="*", choices=[variant.value for variant in Operations])

args = parser.parse_args()


def generate_number_list(n, left_m, left_M, right_m, right_M):
    left_increment = (left_M - left_m) / n
    right_increment = (right_M - left_m) / n

    left_digits = left_m
    right_digits = right_m

    def rand_int(digits): 
        return randrange(
            10**int(digits), 
            10**(int(digits) + 1)
        )

    numbers = []
    for _ in range(n):
        numbers.append(rand_int(left_digits))
        numbers.append(rand_int(right_digits))
        left_digits += left_increment
        right_digits += right_increment

    return numbers

if not os.path.isdir(args.out):
    os.mkdir(args.out)

if args.name:
    file_path = f"{args.out}/{args.name}.txt"
else:
    i = 0
    while os.path.isfile(f"{args.out}/bench-{i}.txt"):
        i += 1
    file_path = f"{args.out}/bench-{i}.txt"
    
def write_header(file, header):
    file.write("\n")
    file.write("=" * 30)
    file.write(header)
    file.write("=" * 30)
    file.write("\n\n")

def write_operation_result(file, operation_name, operation):
    file.write(f"{operation_name}\n")
    for i in range(len(operand_list) // 2):
        result = operation(operand_list[2 * i], operand_list[2 * i + 1])
        file.write(f"{result}\n")
    file.write("\n")

with open(file_path, "w") as file: 
    if args.op:
        file.write(f"OPERATIONS: {args.op}\n")

    write_header(file, "OPERAND LIST")
    
    operand_list = generate_number_list(args.count, args.left[0], args.left[1], args.right[0], args.right[1])

    file.writelines(f"{operand}\n" for operand in operand_list)
    file.write("\n")

    write_header(file, "VERIFICATION")

    for operation in args.op:
        match Operations(operation):
            case Operations.ADDITION:
                write_operation_result(file, "ADDITION", operator.add)
            case Operations.SUBTRACTION:
                write_operation_result(file, "SUBTRACTION", operator.sub)
            case Operations.MULTIPLICATION:
                write_operation_result(file, "MULTIPLICATION", operator.mul)
            case Operations.BIT_AND:
                write_operation_result(file, "BITAND", operator.and_)
            case Operations.BIT_OR:
                write_operation_result(file, "BITOR", operator.or_)
            case Operations.BIT_XOR:
                write_operation_result(file, "XOR", operator.xor)
            case Operations.RIGHT_SHIFT:
                write_operation_result(file, "RSHIFT", operator.rshift)
            case Operations.LEFT_SHIFT:
                write_operation_result(file, "LSHIFT", operator.lshift)