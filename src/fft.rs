/*use std::ops::{Add, Sub, Mul, Neg};
use std::f64::consts::PI;

// http://numbers.computation.free.fr/Constants/Algorithms/fft.html

#[derive(Debug, Clone)]
pub struct Complex {
    real: f64,
    imaginary: f64,
}

impl Complex {
    pub fn new(re: f64, im: f64) -> Complex {
        Complex { real: re, imaginary: im }
    }
    pub fn re(&self) -> f64 {
        return self.real;
    }

    pub fn im(&self) -> f64 {
        return self.imaginary;
    }

    pub fn conj(&mut self) {
        self.imaginary = -self.imaginary;
    }
}

impl Add for Complex {
    type Output = Complex;

    fn add(self, rhs: Self) -> Complex {
        return Complex {
            real: self.real + rhs.real,
            imaginary: self.imaginary + rhs.imaginary,
        }
    }
}

impl Add<f64> for Complex {
    type Output = Complex;

    fn add(self, rhs: f64) -> Complex {
        return Complex {
            real: self.real + rhs,
            imaginary: self.imaginary,
        };
    }
}

impl Sub for Complex {
    type Output = Complex;

    fn sub(self, rhs: Complex) -> Complex {
        return Complex {
            real: self.real - rhs.real,
            imaginary:  self.imaginary - rhs.imaginary
        };
    }
}

impl Mul for Complex {
    type Output = Complex;

    fn mul(self, rhs: Self) -> Complex {
        return Complex {
            real: self.real * rhs.real - self.imaginary * rhs.imaginary,
            imaginary: self.real * rhs.imaginary + rhs.real * self.imaginary,
        }
    }
}

impl Mul<f64> for Complex {
    type Output = Complex;

    fn mul(self, rhs: f64) -> Complex {
        return Complex {
            real: rhs * self.real,
            imaginary: rhs * self.imaginary,
        }
    }
}

enum Sign {
    Positive,
    Negative,
}

struct Exponential {
    numerator: usize,
    denominator: usize,
    sign: Sign,
}

pub fn fft(data: &Vec<f64>, omega: Complex) -> Vec<Complex> {
    if data.len() == 0 {
        return Vec::new();
    }

    let filter_odd_even = |data: &Vec<f64>, n: usize| data.iter()
                                  .enumerate()
                                  .filter(|(i, _)| {i & 1 == n})
                                  .map(|(i, x)| { x })
                                  .cloned()
                                  .collect();

    let data_even = filter_odd_even(data, 0);
    let data_odd = filter_odd_even(data, 1);

    let y_even = fft(&data_even, omega * omega);
    let y_odd = fft(&data_odd, omega * omega);

    let mut y: Vec<Complex> = vec![Complex::new(0.0, 0.0); data.len()];
    let mut omega_n = Complex::new(1.0, 0.0);
    for i in 0..(data.len() / 2) {
        y[i] = y_even[i] + omega_n * y_odd[i];
        y[i + data.len() / 2] = y_even[i] - omega_n * y_odd[i];
        omega_n = omega_n * omega;
    }

    return y;
}


fn ifft<T>(data: &Vec<f64>) -> Vec<f64> where
T : Clone + Neg<Output = T> {
    let theta = 2.0 * PI / (data.len() as f64);
    let omega = Complex::new(theta.cos(), theta.sin());

    data.iter_mut().for_each(|x| { x.conj() });
    fft(data, omega);
    data.iter_mut().for_each(|x| { x.conj() });

    return data.iter().map(|x| { return x / data.len(); })
}
*/
