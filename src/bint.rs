use std::cmp::{min, Eq, Ord, Ordering, PartialEq, PartialOrd};
use std::convert::{From, Into, TryFrom};
use std::fmt::{Binary, Display};
use std::ops::{
    Add, AddAssign, BitAnd, BitAndAssign, BitOr, BitOrAssign, BitXor, BitXorAssign, Div, Mul,
    MulAssign, Neg, Rem, RemAssign, Shl, ShlAssign, Shr, ShrAssign, Sub, SubAssign,
};
use std::rc::Rc;

//use crate::fft::Complex;
use crate::bratio::Bratio;

const RADIX_OFFSET: u64 = 32;
const RADIX: u64 = (2 as u64).pow(RADIX_OFFSET as u32);
const RADIX_MASK: u64 = RADIX - 1;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Sign {
    Positive,
    Negative,
}

impl Neg for Sign {
    type Output = Sign;

    fn neg(self) -> Sign {
        match self == Sign::Positive {
            true => Sign::Negative,
            false => Sign::Positive,
        }
    }
}

impl Mul for Sign {
    type Output = Sign;

    fn mul(self, rhs: Sign) -> Sign {
        match self == rhs {
            true => Sign::Positive,
            false => Sign::Negative,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Bint {
    sign: Sign,
    data: Rc<Vec<u32>>,
}

impl Bint {
    pub fn zero() -> Bint {
        return Bint::from(0);
    }

    pub fn one() -> Bint {
        return Bint::from(1);
    }

    pub fn sign(&self) -> Sign {
        return self.sign;
    }

    pub fn abs(&self) -> Bint {
        Bint {
            sign: Sign::Positive,
            data: self.data.clone(),
        }
    }

    pub fn pow(&self, n: &Bint) -> Bint {
        let mut result = Bint::from(1);
        let mut base = self.clone();
        let mut exp = n.clone();

        while exp != Bint::from(0) {
            if &exp & Bint::from(1) == Bint::from(1) {
                result *= &base;
            }
            let new_base = &base * &base;
            base = new_base;
            exp >>= 1;
        }

        return result;
    }

    /*
        fn fft_multiplication(&self, rhs: Bint) -> Bint {
            let padded_size = 2_usize.pow(
                (std::cmp::max(
                    self.data.len(),
                    rhs.data.len()
                ) as f32).log2().ceil() as u32
            );

            self.data.extend(vec![0; padded_size - self.data.len()]);
            rhs.data.extend(vec![0; padded_size - rhs.data.len()]);


        }
    */

    pub fn euclid_div(&self, other: &Bint) -> (Bint, Bint) {
        if *other == Bint::zero() {
            panic!("Bint division by 0");
        }

        if *other > *self {
            return (Bint::zero(), self.clone());
        }

        if *other == *self {
            return (Bint::one(), Bint::zero());
        }

        let mut n = 0;
        let mut upper_bound_mul = other.clone();

        while upper_bound_mul < *self {
            upper_bound_mul <<= 1;
            n += 1;
        }

        let mut lower_bound_mul = upper_bound_mul.clone() >> 1;

        let mut upper_bound = Bint::from(1) << n;
        let mut lower_bound = upper_bound.clone() >> 1;

        for _ in 1..n {
            let middle = (&lower_bound + &upper_bound) >> 1;
            let middle_mul = (&lower_bound_mul + &upper_bound_mul) >> 1;

            if middle_mul <= *self {
                lower_bound = middle;
                lower_bound_mul = middle_mul;
            } else {
                upper_bound = middle;
                upper_bound_mul = middle_mul;
            }
        }

        return (lower_bound.clone(), self - lower_bound_mul);
    }

    pub fn gcd(&self, other: &Bint) -> Bint {
        if self < other {
            return other.gcd(self);
        }

        let mut a = self.clone();
        let mut b = other.clone();

        while b != Bint::zero() {
            let r = a % &b;
            a = b;
            b = r;
        }

        return a;
    }

    pub fn gcd_div(&self, other: &Bint) -> (Bint, Bint, Bint) {
        if self < other {
            return other.gcd_div(self);
        }

        if *other == Bint::zero() {
            return (self.clone(), Bint::from(1), Bint::zero());
        }

        let (q, r) = self.euclid_div(other);
        let (gcd, kb, kr) = other.gcd_div(&r);

        return (gcd, q * &kb + kr, kb);
    }
}

impl From<i128> for Bint {
    fn from(integer: i128) -> Self {
        if integer == 0 {
            return Bint {
                sign: Sign::Positive,
                data: Rc::new(vec![0]),
            };
        }

        let mut int = integer;
        let mut data = Vec::new();

        while int != 0 {
            data.push((int & RADIX_MASK as i128) as u32);
            int >>= RADIX_OFFSET as u128;
        }

        let sign = match integer < 0 {
            true => Sign::Negative,
            false => Sign::Positive,
        };

        return Bint {
            sign,
            data: Rc::new(data),
        };
    }
}

impl TryFrom<&str> for Bint {
    type Error = String;

    fn try_from(value: &str) -> Result<Bint, String> {
        let mut bigit = 0;
        let chunk_size = 6;
        let mut sign = Sign::Positive;

        let chars: Vec<char> = value.chars().collect();

        let chars_view = match chars.len() > 0 && chars[0] == '-' {
            true => {
                sign = Sign::Negative;
                &chars[1..]
            }
            false => &chars,
        };

        let mut result = Bint::from(0);

        let mut count = 0;
        for c in chars_view {
            match c.to_digit(10) {
                Some(digit) => {
                    bigit *= 10;
                    bigit += digit as u64;
                }
                None => return Err(format!("Invalid digit: {}", c)),
            }

            count += 1;

            if count == chunk_size {
                result *= &Bint::from(10).pow(&Bint::from(chunk_size));
                result += Bint::from(bigit as i128);
                count = 0;
                bigit = 0;
            }
        }

        if bigit != 0 {
            result *= &Bint::from(10).pow(&Bint::from(count));
            result += Bint::from(bigit as i128);
        }

        result.sign = sign;
        return Ok(result);
    }
}

impl TryInto<i128> for Bint {
    type Error = &'static str;

    fn try_into(self) -> Result<i128, &'static str> {
        if self.abs() > Bint::from(i128::MAX) {
            return Err("Cannot convert a Bint larger than i128::MAX to a i128");
        }

        let mut result: i128 = 0;
        for digit in self.data.iter() {
            result <<= RADIX_OFFSET;
            result |= *digit as i128;
        }

        return match self.sign {
            Sign::Positive => Ok(result),
            Sign::Negative => Ok(-result),
        };
    }
}

impl Into<String> for Bint {
    fn into(mut self) -> String {
        let mut result = String::new();
        let base = Bint::from(1_000_000);

        while self != Bint::from(0) {
            let (quotient, remainder) = self.euclid_div(&base);
            let char_chunk: i128 = remainder.try_into().unwrap();

            if quotient != Bint::from(0) {
                result = format!("{char_chunk:06}{result}");
            } else {
                result = format!("{char_chunk}{result}");
            }

            self = quotient;
        }

        return result;
    }
}

impl Neg for Bint {
    type Output = Bint;

    fn neg(self) -> Bint {
        Bint {
            sign: -self.sign,
            data: self.data,
        }
    }
}

impl PartialEq for Bint {
    fn eq(&self, other: &Self) -> bool {
        return self.cmp(other) == Ordering::Equal;
    }
}

impl Eq for Bint {}

impl PartialOrd for Bint {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        return Some(self.cmp(other));
    }
}

impl Ord for Bint {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.data.len() == 1 && self.data[0] == 0 && other.data.len() == 1 && other.data[0] == 0
        {
            return Ordering::Equal;
        }

        if self.sign != other.sign {
            return match self.sign == Sign::Negative {
                true => Ordering::Less,
                false => Ordering::Greater,
            };
        }

        if self.data.len() < other.data.len() {
            return match self.sign == Sign::Negative {
                true => Ordering::Greater,
                false => Ordering::Less,
            };
        } else if self.data.len() > other.data.len() {
            return match self.sign == Sign::Negative {
                true => Ordering::Less,
                false => Ordering::Greater,
            };
        }

        let n = self.data.len();
        for i in 0..n {
            if self.data[n - 1 - i] == other.data[n - 1 - i] {
                continue;
            }

            return match (self.data[n - 1 - i] < other.data[n - 1 - i])
                != (self.sign == Sign::Negative)
            {
                true => Ordering::Less,
                false => Ordering::Greater,
            };
        }

        return Ordering::Equal;
    }
}

impl Add<Bint> for Bint {
    type Output = Bint;

    fn add(self, rhs: Bint) -> Bint {
        return (&self).add(&rhs);
    }
}

impl Add<&Bint> for Bint {
    type Output = Bint;

    fn add(self, rhs: &Bint) -> Bint {
        return (&self).add(rhs);
    }
}

impl Add<Bint> for &Bint {
    type Output = Bint;

    fn add(self, rhs: Bint) -> Bint {
        return self.add(&rhs);
    }
}

impl Add<&Bint> for &Bint {
    type Output = Bint;

    fn add(self, rhs: &Bint) -> Bint {
        let mut result = self.clone();
        result += rhs;
        return result;
    }
}

impl AddAssign<&Bint> for Bint {
    fn add_assign(&mut self, rhs: &Bint) {
        if self.sign == rhs.sign {
            let data = Rc::make_mut(&mut self.data);

            if data.len() < rhs.data.len() {
                data.resize(rhs.data.len(), 0);
            }

            let mut carry: u64 = 0;
            for i in 0..rhs.data.len() {
                let sum = data[i] as u64 + rhs.data[i] as u64 + carry;
                carry = sum >> RADIX_OFFSET;
                data[i] = (sum & RADIX_MASK) as u32;
            }

            for i in 0..(data.len() - rhs.data.len()) {
                let sum = data[rhs.data.len() + i] as u64 + carry;
                carry = sum >> RADIX_OFFSET;
                data[rhs.data.len() + i] = (sum & RADIX_MASK) as u32;
            }

            if carry != 0 {
                data.push(carry as u32);
            }
        } else {
            let sub_data = |left: u32, right: u32, carry: &mut bool| -> u32 {
                let mut left64 = left as u64;
                let mut right64 = right as u64;

                if *carry {
                    right64 += 1;
                }

                if left64 < right64 {
                    left64 += RADIX;
                    *carry = true;
                } else {
                    *carry = false;
                }

                return (left64 - right64) as u32;
            };

            let sub_carry =
                |left_data: &Vec<u32>, right_data: &Vec<u32>, mut carry: bool| -> Vec<u32> {
                    let mut result = Vec::new();

                    for i in right_data.len()..left_data.len() {
                        result.push(rhs.data[i]);

                        if carry {
                            let last = result.last_mut().unwrap();

                            if *last == 0 {
                                *last = (RADIX - 1) as u32;
                            } else {
                                *last -= 1;
                                carry = false;
                            }
                        }
                    }

                    return result;
                };

            if self.abs() < rhs.abs() {
                let data = Rc::make_mut(&mut self.data);

                self.sign = rhs.sign;

                let mut carry = false;
                for i in 0..data.len() {
                    data[i] = sub_data(rhs.data[i], data[i], &mut carry);
                }

                data.extend(sub_carry(&rhs.data, data, carry));
            } else {
                let data = Rc::make_mut(&mut self.data);

                let mut carry = false;
                for i in 0..rhs.data.len() {
                    data[i] = sub_data(data[i], rhs.data[i], &mut carry);
                }

                data.extend(sub_carry(&data, &rhs.data, carry));
            }

            let data = Rc::make_mut(&mut self.data);

            while !data.is_empty() && *data.last().unwrap() == 0 {
                data.pop();
            }
        }
    }
}

impl AddAssign for Bint {
    fn add_assign(&mut self, rhs: Bint) {
        return self.add_assign(&rhs);
    }
}

impl SubAssign<Bint> for Bint {
    fn sub_assign(&mut self, rhs: Bint) {
        self.sub_assign(&rhs);
    }
}

impl SubAssign<&Bint> for Bint {
    fn sub_assign(&mut self, rhs: &Bint) {
        // replace with in place ?
        self.add_assign(&(-rhs.clone()));
    }
}

impl MulAssign<&Bint> for Bint {
    fn mul_assign(&mut self, rhs: &Bint) {
        let mut result = vec![0; 2 * std::cmp::max(self.data.len(), rhs.data.len())];

        for i in 0..self.data.len() {
            for j in 0..rhs.data.len() {
                let mut multiplication = self.data[i] as u64 * rhs.data[j] as u64;

                for digit in &mut result[(i + j)..] {
                    multiplication += *digit as u64;
                    *digit = (multiplication & RADIX_MASK) as u32;
                    multiplication >>= RADIX_OFFSET;

                    if multiplication == 0 {
                        break;
                    }
                }
            }
        }

        while result.len() > 1 && *result.last().unwrap() == 0 {
            result.pop();
        }

        *self = Bint {
            sign: self.sign * rhs.sign,
            data: Rc::new(result),
        };
    }
}

impl Sub<Bint> for Bint {
    type Output = Bint;

    fn sub(self, rhs: Bint) -> Bint {
        return (&self).sub(&rhs);
    }
}

impl Sub<&Bint> for Bint {
    type Output = Bint;

    fn sub(self, rhs: &Bint) -> Bint {
        return (&self).sub(rhs);
    }
}

impl Sub<Bint> for &Bint {
    type Output = Bint;

    fn sub(self, rhs: Bint) -> Bint {
        return self.sub(&rhs);
    }
}

impl Sub<&Bint> for &Bint {
    type Output = Bint;

    fn sub(self, rhs: &Bint) -> Bint {
        let mut result = self.clone();
        result -= rhs;
        return result;
    }
}

impl Mul for Bint {
    type Output = Bint;

    fn mul(self, rhs: Bint) -> Bint {
        return (&self).mul(&rhs);
    }
}

impl Mul<&Bint> for Bint {
    type Output = Bint;

    fn mul(self, rhs: &Bint) -> Bint {
        return (&self).mul(rhs);
    }
}

impl Mul<Bint> for &Bint {
    type Output = Bint;

    fn mul(self, rhs: Bint) -> Bint {
        return self.mul(&rhs);
    }
}

impl Mul<&Bint> for &Bint {
    type Output = Bint;

    fn mul(self, rhs: &Bint) -> Bint {
        let mut result = self.clone();
        result *= rhs;
        return result;
    }
}

impl Div for Bint {
    type Output = Bratio;

    fn div(self, rhs: Bint) -> Bratio {
        return (&self).div(&rhs);
    }
}

impl Div<&Bint> for Bint {
    type Output = Bratio;

    fn div(self, rhs: &Bint) -> Bratio {
        return (&self).div(rhs);
    }
}

impl Div<Bint> for &Bint {
    type Output = Bratio;

    fn div(self, rhs: Bint) -> Bratio {
        return self.div(&rhs);
    }
}

impl Div<&Bint> for &Bint {
    type Output = Bratio;

    fn div(self, rhs: &Bint) -> Bratio {
        return Bratio::new(self.clone(), rhs.clone());
    }
}

impl Rem for Bint {
    type Output = Bint;

    fn rem(self, rhs: Bint) -> Bint {
        return (&self).rem(&rhs);
    }
}

impl Rem<&Bint> for Bint {
    type Output = Bint;

    fn rem(self, rhs: &Bint) -> Bint {
        return (&self).rem(rhs);
    }
}

impl Rem<Bint> for &Bint {
    type Output = Bint;

    fn rem(self, rhs: Bint) -> Bint {
        return self.rem(&rhs);
    }
}

impl Rem for &Bint {
    type Output = Bint;

    fn rem(self, rhs: &Bint) -> Bint {
        let mut result = self.clone();
        result %= rhs;
        return result;
    }
}

impl BitAnd for Bint {
    type Output = Bint;

    fn bitand(self, rhs: Bint) -> Bint {
        return (&self).bitand(&rhs);
    }
}

impl BitAnd<&Bint> for Bint {
    type Output = Bint;

    fn bitand(self, rhs: &Bint) -> Bint {
        return (&self).bitand(rhs);
    }
}

impl BitAnd<Bint> for &Bint {
    type Output = Bint;

    fn bitand(self, rhs: Bint) -> Bint {
        return self.bitand(&rhs);
    }
}

impl BitAnd<&Bint> for &Bint {
    type Output = Bint;

    fn bitand(self, rhs: &Bint) -> Bint {
        let mut result = self.clone();
        result &= rhs;
        return result;
    }
}

impl BitOr for Bint {
    type Output = Bint;

    fn bitor(self, rhs: Bint) -> Bint {
        let mut result = self.clone();
        result |= rhs;
        return result;
    }
}

impl BitXor for Bint {
    type Output = Bint;

    fn bitxor(self, rhs: Bint) -> Bint {
        let mut result = self.clone();
        result ^= rhs;
        return result;
    }
}

impl Shr<u128> for Bint {
    type Output = Bint;

    fn shr(self, rhs: u128) -> Bint {
        let mut result = self.clone();
        result >>= rhs;
        return result;
    }
}

impl Shl<u128> for Bint {
    type Output = Bint;

    fn shl(self, rhs: u128) -> Bint {
        let mut result = self.clone();
        result <<= rhs;
        return result;
    }
}

impl RemAssign for Bint {
    fn rem_assign(&mut self, rhs: Bint) {
        self.rem_assign(&rhs);
    }
}

impl RemAssign<&Bint> for Bint {
    fn rem_assign(&mut self, rhs: &Bint) {
        let (_, r) = self.euclid_div(rhs);
        *self = r;
    }
}

impl BitAndAssign<&Bint> for Bint {
    fn bitand_assign(&mut self, rhs: &Bint) {
        let data = Rc::make_mut(&mut self.data);

        let new_len = min(data.len(), rhs.data.len());
        for i in 0..new_len {
            data[i] &= rhs.data[i];
        }
        data.resize(new_len, 0);
    }
}

impl BitAndAssign for Bint {
    fn bitand_assign(&mut self, rhs: Bint) {
        self.bitand_assign(&rhs);
    }
}

impl BitOrAssign for Bint {
    fn bitor_assign(&mut self, rhs: Bint) {
        let data = Rc::make_mut(&mut self.data);

        if data.len() < rhs.data.len() {
            data.resize(rhs.data.len(), 0);
        }

        for i in 0..min(data.len(), rhs.data.len()) {
            data[i] |= rhs.data[i];
        }
    }
}

impl BitXorAssign for Bint {
    fn bitxor_assign(&mut self, rhs: Bint) {
        let data = Rc::make_mut(&mut self.data);

        if data.len() < rhs.data.len() {
            data.resize(rhs.data.len(), 0);
        }

        for i in 0..min(data.len(), rhs.data.len()) {
            data[i] ^= rhs.data[i];
        }
    }
}

impl ShrAssign<u128> for Bint {
    fn shr_assign(&mut self, rhs: u128) {
        let data = Rc::make_mut(&mut self.data);

        let n = rhs.div_euclid(RADIX_OFFSET as u128) as usize;
        let r = (rhs - n as u128 * RADIX_OFFSET as u128) as u32;

        if n > 0 {
            for i in n..data.len() {
                data.swap(i, i - n);
            }

            data.resize(data.len() - n, 0);
        }

        if r > 0 {
            let mut carry = 0;
            let carry_mask = (1 << r) - 1;

            for digit in data.iter_mut().rev() {
                let new_carry = (*digit & carry_mask) << (RADIX_OFFSET as u32 - r);
                *digit = (*digit >> r) | carry;
                carry = new_carry;
            }
        }
    }
}

impl ShlAssign<u128> for Bint {
    fn shl_assign(&mut self, rhs: u128) {
        let data = Rc::make_mut(&mut self.data);

        let n = rhs.div_euclid(RADIX_OFFSET as u128) as usize;
        let r = (rhs % RADIX_OFFSET as u128) as u32;

        if n > 0 {
            let old_len = data.len();
            data.resize(data.len() + n, 0);

            for i in (0..old_len).rev() {
                data.swap(i, i + n);
            }
        }

        if r > 0 {
            let mut carry = 0;
            let carry_mask = ((1 << r) - 1) << (RADIX_OFFSET as u32 - r);

            for digit in data {
                let new_carry = (*digit & carry_mask) >> (RADIX_OFFSET as u32 - r);
                *digit = (*digit << r) | carry;
                carry = new_carry;
            }

            let data = Rc::make_mut(&mut self.data);

            if carry != 0 {
                data.push(carry);
            }
        }
    }
}

impl Display for Bint {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let string: String = self.clone().into();
        return write!(f, "{}", string);
    }
}

impl Binary for Bint {
    fn fmt(&self, formatter: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut iterator = self.data.iter().rev();
        let mut buffer = format!("{:b}", iterator.next().unwrap());

        for digit in iterator {
            buffer = format!("{buffer}{digit:00$b}", RADIX_OFFSET.clone() as usize);
        }

        return formatter.pad_integral(true, "0b", &buffer);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn bint_from_int() {
        let n0 = Bint::from(38);
        assert_eq!(n0.sign, Sign::Positive);
        assert_eq!(n0.data, Rc::new(vec![38]));
    }

    #[test]
    fn bint_from_str() {
        let n = Bint::try_from("-56").unwrap();
        assert_eq!(n.sign, Sign::Negative);
        assert_eq!(n.data, Rc::new(vec![56]));
    }

    #[test]
    fn bint_to_string() {
        let n = Bint::try_from("7832907354839025432980263450986528726348984209387584920847345054786042093485785905479034").unwrap();
        assert_eq!(n.to_string(), "7832907354839025432980263450986528726348984209387584920847345054786042093485785905479034");
    }

    #[test]
    fn bint_add_small() {
        let a = Bint::from(10);
        let b = Bint::from(47);
        let c = a + b;
        assert_eq!(c.data, Rc::new(vec![57]));
    }

    #[test]
    fn bint_add_big() {
        let a = Bint::try_from("74398016234097890534435413244321987534").unwrap();
        let b = Bint::try_from("4473890126544381290457843165872019349831908743901561324").unwrap();
        let c = a + b;
        assert_eq!(
            c.to_string(),
            "4473890126544381364855859399969909884267321988223548858"
        )
    }

    #[test]
    fn bint_add_negative_small() {}

    #[test]
    fn bin_add_negative_big() {}

    #[test]
    fn bint_multiply_small() {}

    #[test]
    fn bint_multiply_big() {
        let a =
            Bint::try_from("443271902345674901032981965490235941898347547345900043829154").unwrap();
        let b = Bint::try_from(
            "978097843165804023457486319309450878903249807865903980752390459879805324",
        )
        .unwrap();
        let c = a * b;
        assert_eq!(c.to_string(), "433563291620307525974164998602716633714828182948122542284316136014886689161896997051143073974729070294543693334922223774409035615896")
    }

    #[test]
    fn bint_multiply_1negative_small() {}

    #[test]
    fn bint_multiply_2negative_small() {}

    #[test]
    fn bint_shift_left_small() {
        let a = Bint::from(0xF0000);
        let b = a.clone() >> 4;
        let c = a << 4;

        assert_eq!(b.to_string(), "61440");
        assert_eq!(c.to_string(), "15728640");
    }

    #[test]
    fn bint_shift_left() {
        let a = Bint::from(0xF00000000000);
        let b = a.clone() >> 4;
        let c = a << 4;

        assert_eq!(b.to_string(), "16492674416640");
        assert_eq!(c.to_string(), "4222124650659840");
    }
}
