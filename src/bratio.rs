use std::cmp::{Eq, Ord, Ordering, PartialEq, PartialOrd};
use std::convert::{From, TryInto};
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

use crate::bint::{Bint, Sign};

// https://webusers.imj-prg.fr/~christian.blanchet/enseignement/2007-8_S2/ch1_div_euclidienne.pdf

#[derive(Debug, Clone)]
pub struct Bratio {
    numerator: Bint,
    denominator: Bint,
}

impl Bratio {
    pub fn new(numerator: Bint, denominator: Bint) -> Self {
        if denominator.sign() == Sign::Negative {
            return Self {
                numerator: -numerator,
                denominator: -denominator,
            };
        }

        return Self {
            numerator,
            denominator,
        };
    }

    pub fn reduce(&mut self) {
        if self.numerator < self.denominator {
            return;
        }

        let (_, ka, kb) = self.numerator.gcd_div(&self.denominator);

        self.numerator = ka;
        self.denominator = kb;
    }

    pub fn is_integer(&self) -> bool {
        return self.numerator.clone() % self.denominator.clone() == Bint::zero();
    }

    pub fn quotient(&self) -> Bint {
        let (quotient, _) = self.numerator.euclid_div(&self.denominator);
        return quotient;
    }
}

impl From<f64> for Bratio {
    fn from(mut value: f64) -> Self {
        let mut n1 = Bint::one();
        let mut d1 = Bint::zero();
        let mut n2 = Bint::zero();
        let mut d2 = Bint::one();

        loop {
            let a = Bint::from(value.trunc() as i128);

            let n = &n1 * &a + n2;
            let d = &d1 * &a + d2;

            n2 = n1;
            d2 = d1;
            n1 = n;
            d1 = d;

            // TODO: find a better way and replace this arbitrary value
            if value.fract() < 0.000001 {
                break;
            }

            value = 1.0 / value.fract();
        }

        return Bratio::new(n1, d1);
    }
}

impl TryInto<f64> for Bratio {
    type Error = String;

    fn try_into(self) -> Result<f64, Self::Error> {
        // division euclidienne ?
        // si le q est trop grand on retourne une erreur
        // algorithme pour calculer la partie décimale r / q:
        // - si q.len() - r.len() > 2 retourner q (on considére qu'il n'y a pas de partie décimale)
        // - on rassemble les deux u32-digit de poids fort de q pour former un u64 puis un f64, noté d
        // - on rassemble le (si q.len() - r.len() == 1) ou les deux (si q.len() - r.len() == 0) u32-digit de poids fort de r pour former un u64 puis un f64 noté n
        // - on retourne le q + n/d
        todo!();
    }
}

impl PartialEq for Bratio {
    fn eq(&self, other: &Self) -> bool {
        self.cmp(other) == Ordering::Equal
    }
}

impl Eq for Bratio {}

impl PartialOrd for Bratio {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Bratio {
    fn cmp(&self, other: &Self) -> Ordering {
        let discriminant = self.numerator.clone() * other.denominator.clone()
            - self.denominator.clone() * other.numerator.clone();

        if discriminant == Bint::zero() {
            return Ordering::Equal;
        }

        match discriminant.sign() {
            Sign::Positive => Ordering::Greater,
            Sign::Negative => Ordering::Less,
        }
    }
}

impl Neg for Bratio {
    type Output = Bratio;

    fn neg(self) -> Bratio {
        Bratio {
            numerator: -self.numerator,
            denominator: self.denominator,
        }
    }
}

impl Add for Bratio {
    type Output = Bratio;

    fn add(mut self, rhs: Self) -> Bratio {
        self.add_assign(rhs);
        return self;
    }
}

impl Sub for Bratio {
    type Output = Bratio;

    fn sub(mut self, rhs: Self) -> Bratio {
        self.sub_assign(rhs);
        return self;
    }
}

impl Mul for Bratio {
    type Output = Bratio;

    fn mul(mut self, rhs: Self) -> Bratio {
        self.mul_assign(rhs);
        return self;
    }
}

impl AddAssign<&Bratio> for Bratio {
    fn add_assign(&mut self, rhs: &Bratio) {
        if self.denominator == rhs.denominator {
            self.numerator += &rhs.denominator;
            return;
        }

        self.numerator = &self.numerator * &rhs.denominator + &rhs.numerator * &self.denominator;
        self.denominator *= &rhs.denominator;
    }
}

impl AddAssign<Bratio> for Bratio {
    fn add_assign(&mut self, rhs: Bratio) {
        self.add_assign(&rhs);
    }
}

impl SubAssign<&Bratio> for Bratio {
    fn sub_assign(&mut self, rhs: &Bratio) {
        if self.denominator == rhs.denominator {
            self.numerator -= &rhs.denominator;
            return;
        }

        self.numerator = &self.numerator * &rhs.denominator - &rhs.numerator * &self.denominator;
        self.denominator *= &rhs.denominator;
    }
}

impl SubAssign<Bratio> for Bratio {
    fn sub_assign(&mut self, rhs: Bratio) {
        self.sub_assign(&rhs);
    }
}

impl MulAssign<&Bratio> for Bratio {
    fn mul_assign(&mut self, rhs: &Bratio) {
        self.numerator *= &rhs.numerator;
        self.denominator *= &rhs.denominator;
    }
}

impl MulAssign for Bratio {
    fn mul_assign(&mut self, rhs: Bratio) {
        self.mul_assign(&rhs);
    }
}

impl DivAssign<&Bratio> for Bratio {
    fn div_assign(&mut self, rhs: &Bratio) {
        self.numerator *= &rhs.denominator;
        self.denominator *= &rhs.numerator;
    }
}

impl DivAssign for Bratio {
    fn div_assign(&mut self, rhs: Bratio) {
        self.div_assign(&rhs);
    }
}

impl Div for Bratio {
    type Output = Bratio;

    fn div(mut self, rhs: Self) -> Bratio {
        self.div_assign(rhs);
        return self;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn from_decimal() {
        let a = Bratio::from(0.5);
        assert_eq!(a.numerator, Bint::from(1));
        assert_eq!(a.denominator, Bint::from(2));

        let b = Bratio::from(3.141592920353982);
        assert_eq!(b.numerator, Bint::from(355));
        assert_eq!(b.denominator, Bint::from(113));
    }
}
